<!--
SPDX-FileCopyrightText: 2021 KDE e.V. <https://ev.kde.org/>

SPDX-License-Identifier: CC-BY-SA-4.0
-->


# Table of Contents

1.  [Resources On Blauer Engel Eco-Certification & Sustainable Software Development](#org8844ede)
    1.  [Blauer Engel ('Blue Angel') Eco-Certification](#orgfcf1c86)
        1.  [Application Information](#org0ecb222)
        2.  [Related Publications](#orga3d7d5f)
        3.  [Related Videos](#orgd3ec162)
    2.  [News & Opinions](#org8cb9dea)
        1.  [Blog Posts, Etc.](#org6b4f55d)
    3.  [Other Organizations](#org3391611)


<a id="org8844ede"></a>

# Resources On Blauer Engel Eco-Certification & Sustainable Software Development

Note that all links have been archived with the [Wayback Machine](https://web.archive.org/) to prevent link rot.


<a id="orgfcf1c86"></a>

## Blauer Engel ('Blue Angel') Eco-Certification


<a id="org0ecb222"></a>

### Application Information

-   [Resource and Energy-EfficientSoftware Products (website)](https://www.blauer-engel.de/en/products/electric-devices/resources-and-energy-efficient-software-products) / [Ressourcen- und energieeffiziente Softwareprodukte (website)](https://www.blauer-engel.de/de/produktwelt/elektrogeraete/ressourcen-und-energieeffiziente-softwareprodukte)
-   [Basic Award Criteri (pdf)](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf) / [Vergabekriterien (pdf)](https://produktinfo.blauer-engel.de/uploads/criteriafile/de/DE-UZ%20215-202001-de%20Kriterien-2020-01-16.pdf)
-   [Application Documents (zip)](https://produktinfo.blauer-engel.de/uploads/raluz_uz/DE-UZ%20215-202001-en-Software%20Products-2020-07-07.zip) / [Antragsunterlagen (zip)](https://produktinfo.blauer-engel.de/uploads/raluz_uz/DE-UZ%20215-202001-de-Softwareprodukte-2020-07-07.zip)
-   [Online Beantragug (German only)](https://portal.ral-umwelt.de/)


<a id="orga3d7d5f"></a>

### Related Publications

1.  Desktop Software (Current Criteria)

    -   Eva Kern, Lorenz M.Hilty, Achim Guldner, Yuliyan V. Maksimov, Andreas Filler, Jens Gröger, Stefan Nauman, 2018. [Sustainable software products—Towards assessment criteria for resource and energy efficiency](https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/1-s2.0-S0167739X17314188-main.pdf)
    -   Jens Gröger, 2019. [Leitfaden zur umweltfreundlichen öffentlichen Beschaffung von Software](https://www.umweltbundesamt.de/publikationen/leitfaden-zur-umweltfreundlichen-oeffentlichen-21) ('Guide to green public procurement of software')
    -   Jens Gröger, Dr. Andreas Köhler, Dr. Stefan Naumann, Andreas Filler, Achim Guldner, Eva Kern, Dr. Lorenz M. Hilty, Yuliyan Maksimov, 2018. [Entwicklung und Anwendung von Bewertungsgrundlagen für ressourceneffiziente Software unter Berücksichtigung bestehender Methodik](https://www.umweltbundesamt.de/publikationen/entwicklung-anwendung-von-bewertungsgrundlagen-fuer) ('Development and application of evaluation principles for resource-efficient software taking into account existing methodology')

2.  Cloud Computing (Future Criteria)

    -   Jens Gröger, Dr. Lutz Stobbe, Jan Druschke, Nikolai Richte, 2021. [Green Cloud Computing: Lebenszyklusbasierte Datenerhebung zu Umweltwirkungen des Cloud Computing](https://www.umweltbundesamt.de/publikationen/green-cloud-computing) ('Green Cloud Computing: Lifecycle-based data collection on environmental impacts of cloud computing')
    -   [Treibhauseffekt von Streaming, Videokonferenz & Co berechenbar](https://www.umweltbundesamt.de/themen/treibhauseffekt-von-streaming-videokonferenz-co) ('Greenhouse effect of streaming, video conferencing & co calculable')


<a id="orgd3ec162"></a>

### Related Videos

-   Cornelius Schumacher, 2021. [*Towards sustainable computing*](https://mirror.kumi.systems/kde/files/akademy/2021/akademy2021-30-Towards-sustainable-computing.mp4)
-   Marina Köhn & Dr. Eva Kern, 2019. [*Wie klimafreundlich ist Software? Einblicke in die Forschung und Ausblick in die Umweltzertifizierung!*](https://media.ccc.de/v/36c3-10852-wie_klimafreundlich_ist_software) ('How climate-friendly is software? Insights into research and outlook on environmental certification!'). Original in German, dubbing available for English and French


<a id="org8cb9dea"></a>

## News & Opinions


<a id="org6b4f55d"></a>

### Blog Posts, Etc.

-   [Notes from Akademy 2021's BoF on energy efficiency](https://mail.kde.org/pipermail/energy-efficiency/2021-June/000003.html)
-   Erik Alber's [Sustainable Software Blog](https://blog.3rik.cc/tag/sustainable-software/) (German) (no longer updated)
-   Philipp Trommler, 2019. [My Opinion on the "Blauer Engel" for Software](https://blog.philipp-trommler.me/posts/2019/12/30/opinion-blauer-engel-software/)
-   [Sustainable Software Blog](https://sustainablesoftware.blogspot.com) (last post 2019)


<a id="org3391611"></a>

## Other Organizations

-   [Runde Tisch Reparatur (RTR)](https://en.runder-tisch-reparatur.de/) (English, German, French)
    -   ["Encouraging Repair: Reducing Resource Consumption and Encouraging Local Economic Development"](https://en.runder-tisch-reparatur.de/wp-content/uploads/2016/11/Positionspapier-englisch-layoutet-_-11-11-16-1.pdf)
    -   [Blog](https://runder-tisch-reparatur.de/category/allgemein/) (German only)

-   [Global Ecolabelling Network](https://www.globalecolabelling.net/)
    -   [List](https://www.blauer-engel.de/en/blue-angelpartners/global-ecolabelling-network) from Blauer Engel website


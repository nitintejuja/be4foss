<!--
SPDX-FileCopyrightText: 2021 KDE e.V. <https://ev.kde.org/>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

All activities of the Blauer Engel for FOSS project will follow the KDE Code of Conduct:

-   <https://www.kde.org/code-of-conduct/>

In case of problems please contact the KDE e.V. Community Working Group:

-   <https://ev.kde.org/workinggroups/cwg.php>


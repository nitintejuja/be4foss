---
author: KDE Eco
date: 6 March 2023
title: "Applying The Blue Angel Criteria To Free Software"
subtitle: "A Handbook To Certifying Software As Sustainable (February 2023)"
date: "2023-03-06"
lang: "en"
toc : true
toc-own-page : true
toc-depth : 2
colorlinks: true
header-right: "KDE Eco"
footer-left: "\\\ "
footer-right: "\\\ "
footer-center: "\\thepage"
footnotes-pretty: true
float-placement-figure: ht
---


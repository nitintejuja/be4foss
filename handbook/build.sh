#!/usr/bin/env bash
set -e

echo "     
⠀⠀⠀⠀⠀⠀⠀⠀⣴⣾⣿⣿⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⢠⣤⣤⣄⣾⣿⣿⣿⡿⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⢸⣿⣿⣿⣿⣿⣟⣛⠁⠀⣀⣀⡀⠀⠀⠀
⠀⠀⠀⠀⠙⣛⠛⣿⣿⣿⣿⢀⣴⣿⣿⡿⠁⠀⠀  _  _____  ___   ___ ___ ___  
⠀⠀⠀⠀⢺⣿⣿⣿⣿⣿⣿⣾⣿⡿⠋⠀⠀⠀⠀ | |/ /   \| __| | __/ __/ _ \ 
⠀⠀⠀⢀⣀⣿⡿⠉⣿⣿⣿⣿⣿⣧⡀⠀⠀⠀⠀ | ' <| |) | _|  | _| (_| (_) |
⠀⠀⠀⣿⣿⣿⡇⠀⣿⣿⣿⠙⣿⣿⣿⣄⠀⠀⠀ |_|\_\___/|___| |___\___\___/
⠀⠀⠀⠀⢈⣿⣷⣀⠿⠿⠟⣀⣾⣿⠿⠛⠀⠀⠀ Handbook Generator
⠀⠀⠀⠀⠺⣿⡿⠻⣿⣿⣿⠟⢿⣿⠗⠀⠀⠀
"

echo "⚙ Checking dependencies"
dep=( latex pandoc gs)

for i in "${dep[@]}"
do
	if command -v "$i" >/dev/null 2>&1; then echo "✔ $i" ; else echo "⚠ $i"; fi
done
echo

echo "🌱 Setting up publish directory..."
mkdir -p ./publish
mkdir -p ./publish/temp
cp -R ./sections/images ./publish/

echo "🌱 Concatenatig markdown...(PDF)"
cat sections/0.0_front-matter.md \
	resources/typesetdate.md \
	sections/0.1_authors-license.md \
	resources/newpage.tex \
	sections/0.2_intro.md \
	resources/newpage.tex \
	sections/1_material-footprint.md \
	resources/newpage.tex \
	sections/2_BE-Okular.md \
	resources/newpage.tex \
	sections/3_criteria-guide.md \
	resources/newpage.tex \
	sections/4_back-matter.md \
	>./publish/handbook.md

echo "🌱 Concatenatig markdown...(Web)"
cat sections/0.0_front-matter.md \
		sections/0.2_intro.md \
		sections/1_material-footprint.md \
		sections/2_BE-Okular.md \
		sections/3_criteria-guide.md \
		sections/0.1_authors-license.md \
		sections/4_back-matter.md \
	>./publish/handbook_web.md

echo "🌱 Generating PDF with cover..."
cd publish
pandoc "handbook.md" -o "./temp/handbook.pdf" --from markdown -H "../resources/custom-settings.tex" --template eisvogel --listings \
	-V mainfont="Times" -V colorlinks=true -V linkcolor=blue -V urlcolor=blue -V toccolor=black -B ../resources/include-cover.tex

echo "🌱 Optimizing PDF..."
# Replace -dPDFSETTINGS=/printer -dColorImageResolution=<NUMBER> with -dPDFSETTINGS='/ebook' for a smaller but lower quality PDF
gs -q -dNOPAUSE -sDEVICE=pdfwrite -dPDFSETTINGS=/printer -dColorImageResolution=150 -sOUTPUTFILE=$(date -d "today" +"%Y-%m-%d")\_sustainable-software-handbook_kde-eco.pdf -dBATCH ./temp/handbook.pdf

echo "🌱 Cleaning up..."
cd ./..
rm -rf ./publish/temp ./publish/handbook.md ./publish/images/cover-image.png

echo "🎉 Done! Handbook has been generated under 'publish'"

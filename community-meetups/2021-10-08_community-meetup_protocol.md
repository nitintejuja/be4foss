Minutes for the call with the community on 2021-10-08.

Start: 13:00
End: 14:00

17 Participants + 2 BE4FOSS organizers [names removed to protect participant privacy]

Notes:

* Intro to the meeting, agenda, etc
    * See pdf of etherpad notes "2021-10-08_community-meetup_pad_measurement-lab-checklist.pdf" sent around before meetup.
* Round of introductions (via chat)
* Presentation of Umweltcampus about their measurement lab:
    * Installing software stack on computer
    * Using a standardized using scenario
    * Measure energy usage with voltmeter Janitza UMG 604, possible on 1 computer but suggests to use 2 to check data while measuring
    * Shows what the measurement (software and hardware) data looks like (e.g., with Collectl: CPU usage, network usage…)
    * OSCAR is used to create a report of the data
    * Data from the power meter already gives average
    * Clear all the cache before starting again by reinstalling the software and clearing the RAM
        * Suggestion to use docker for this (also helpful with client-server software, but overhead of virtual machine etc)
        * Clonezilla with software installed that is needed, also good for replicating results
    * Question about error margins? accuracy specified to 1% error per measurement in Juantiza (?)
* Community member presents emulation software options: xdotool, Xnee, PyAutiGUI, Atbswp, Actiona, Sikulix, Squish, Eggplant Funtional, PuloversMacroCreator, WinAutomation
    * Question to community: does someone want to write a script for extracting the list of actions out of, e.g., Actiona?
    * Issue of different monitor sizes for pixel-based tools
    * KXMLGUI could help to automate usage scenarios for KDE applications
    * Community member is comparing energy usage with different automation techniques in master thesis, may present results at future meetup
        * Cross-validation of emulation techniques may depend on software being used
* Question to community: who wants to help to set up a measurement lab in Berlin or online (see chat protocol), another meetup to discuss will be set up
* Next meetup 10 November at 19:00 CEST with the topic: "Planning on how to automate standard usage scenarios"
* Future meetups always 2nd Wednesday of every month at 19:00 CEST

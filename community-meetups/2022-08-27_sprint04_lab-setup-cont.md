
# Table of Contents

1.  [DAY OVERVIEW](#org9113998)
2.  [GETTING STARTED](#org9d58be1)
3.  [AFTERNOON ACTIVITIES](#org259e3ec)
4.  [WRAP UP](#org9f244ec)


<a id="org9113998"></a>

# DAY OVERVIEW


## Plan

-   The primary goals of this Sprint were:
    - 1. Resolve issues related to the Gude power meter readout.
    - 2. Begin measuring KDE/Free Software. 
-   When: 27 August 2022, 11-19h
-   Schedule (outline):
    - 11:00-12:30 preparation
    - 12:30-13:30 lunch
    - 13:30-16:30 check Gude power meter scripts & start measurement tests
    - 16:30-17:00 break
    - 17:00-19:00 continue measurements, wrap & plan next steps 
-   Relevant pre-Sprint links:
    -   GUDE power meter script (preliminary): <https://github.com/green-coding-berlin/green-metrics-tool/tree/dev/tools/metric_providers/psu/energy/ac/system>
    -   Kate SUS: <https://invent.kde.org/teams/eco/be4foss/-/tree/master/standard-usage-scenarios/kate>

## Achieved

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">TIME</th>
<th scope="col" class="org-left">ACHIEVED</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">11:00-12:30</td>
<td class="org-left">- Introductions, planning, and preparation</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">12:30-13:30</td>
<td class="org-left">- Lunch</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">13:30-15:00</td>
<td class="org-left">- Reviewing GUDE Python script resolving readout issue</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">- Demo of Green Metrics Tool output </td>
</tr>

</tbody>

<tbody>
<tr>
<td class="org-right">15:00-17:30</td>
<td class="org-left">- Video call with virtual participant</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">- Testing GUDE Python script in lab</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">- Merge Request for final script at FEEP repo</td>
</tr>


<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">- Measurement test with Kate SUS</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">17:30-18:30</td>
<td class="org-left">- Updating and testing Gosund SP111 guide</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">18:30-18:45</td>
<td class="org-left">- Wrap up, clean up, and lock up</td>
</tr>
</tbody>

</table>

-   Total of 4 in-person participants, 1 virtual participant [names removed to protect participant privacy]

-   Relevant post-Sprint links:
    -   GUDE power meter script: <https://invent.kde.org/teams/eco/feep/-/tree/master/tools/GUDEPowerMeter>
    -   Gosund SP111 notes: <https://invent.kde.org/teams/eco/feep/-/blob/master/tools/gosund-sp111-notes.md>

<a id="org9d58be1"></a>

# GETTING STARTED


## 11:00-12:30

-   Warm up, chat

-   Lab location & access discussion:
    -   Q: Can we move the lab to the other side of office or put it in storage room.
        -   Comment: Issue of other use of space such as storing customer stuff
    -   Q: Will access eventually be remote or stay local? How often will we need to give access? How many people will use the lab?
        -   Comment: For remote access, PM and systems under test have been moved to training network from internal network, matter of setting up isolation and routing for external access
        -   Comment: No need to give keys, guest researchers can come by during office hours, but may want communication channel to check if someone is in the office. Important: guests cannot be the last one in building or may trigger alarm

-   Power meter discussion:
    -   PM not made for high resolution readout, Microchip MCP39F511N device is made for that with higher time resolution
    -   Would Blauer Engel accept other PMs? Probably. GUDE is a good off-the-shelf solution, probably why it was chosen, but maybe other PMs are also good
    -   Problem: There is a EUR 10 device which gives sufficient precision (1 kW resolution? two separate channels?) or EUR 1000 device which is more than we need, but nothing in between
    -   Issue of electical safety for mid-range price devices (e.g., Microchip MCP39F511N), possible insurance issue. Also there is the issues of availability and "idiosyncronicity" of devices
    -   For today: Check solutions and identify remaining issues
    -   What about non-free solutions? Perhaps can be made FOSS by reverse engineering the software, can observe output
    -   Is it really an issue? Many devices provide interpolation/estimation of results, not observations anyway

-   Gosund power plug
    -   Today: Work on documentation and LabPlot visualization
    -   Modify firmware for more robust high frequency readout?
    -   Interacting when wifi is broken, write a how-to for NOT breaking device
    -   Device may no longer be available, but there are alternatives
    -   Q: BoF at Akademy?
        -   Comment: Issue of local WiFi setup, need devices on same network to be able to communicate with each other (does wifi setup allow direct communication of two clients?), perhaps just need to bring router to make wifi access point (our own wifi access point)
        -   Bring Gude PM to a BoF?
        -   Note: need lamp and non-LED bulb for calibration, since calibration is needed when changing wifi networks or resetting the devices

-   Information about handbook planning
    -   Currently the handbook will be in three parts: 1. Introduction to software-driven energy consumption; 2. Blue Angel eco-label for resource-efficient software design, Okular case study; 3. A how-to for measuring the energy consumption of one's own software
    -   To be published as a PDF and online at our website
    -   Website can be updated regularly by community, curated version of which will be released once a year or once every two years

-   Sustainable cronjobs
    -   The site electricitymap.com aggregates data for power grid, but proprietary
    -   But scripts are open source, works for countries like Bolivia, but not Germany (requires signing up, etc.)
    -   There are few cases where there is just an API giving results, some of the data has to be extracted in challenging ways (reading numbers off of a graph)
    -   Currently some data is available, but there remain issues of normalization of raw data, forecasting data, continuous maintanenance of the data, offering a stable point of access, remove access barriers (e.g., having to write emails to get access)

-   Today's plan:
    -   GUDE PM: Work on readout issue
    -   Gosund Power Plug: documentation, labplot visualization, fixing broken devices
    -   Time-permitting: work on power grid data / sustainable cronjobs
    -   To keep on the radar: Accessibility interface for remote control (Selenium), also relevant for Plasma; see <https://mail.kde.org/pipermail/energy-efficiency/2022-August/000097.html>


<a id="org259e3ec"></a>

# AFTERNOON ACTIVITIES


## 13:30-15:00

-   Gude Power Meter
    -   Issue: running script doesn't capture all activity, not high enough resolution to capture small changes, not as fine-grained as we may want; slight delay in readout
    -   Re time shift issue: not problematic for Blauer Engel per se since there mainly looking for improvements/regressions; however, problematic for identifying specific areas causing increases in power consumption
    -   Checked calibration: output is not last value measured but average of samples (60 watt bulb showing 30 watt when flicked on-and-off under 1-second)
    -   Note: in script, readout is converted to millijoule / microjoules (written in script); original readout is in watts.
    -   See blog post updating DC measurement at Green Coding Berlin: <https://www.green-coding.org/blog/adventures-in-dc-measurement/>

-   Green Metrics Tool
    -   atx_energy_dc_channel [J] (on mainboard connectors) also shows 4 time-tick delay, perhaps processing delay of the chips. Should be a stable offset, can be compensated.
    -   millisecond delay with RAPL, whereas psu_energy_ac_system shows a long delay (1.2 seconds!)
    -   ATX is higher and PSU AC energy is even higher
    -   Could not package Okular in a container, thus can only look at system wide metrics
    -   Delay of 1.2 seconds after Okular opened in psu_energy_ac_system [J]
    -   ATX power is not comparable to PSU AC power because in different timeframes
    -   Delay varies within its own resolution in update function
    -   Script runs at 33 ms intervals, but likely due to weak wifi signal, in cabled lab maybe this is not a problem
    -   Demo: services and flow (xdotool containter, run bash script)
    -   Note: Okular script expects a file at least 60 pages

-   Labplot live-plotting notes:
    -   Redirect script output to CSV file
    -   Import CSV file into labplot: *File* > *Add New* > *Live Data Source ...*
    -   Select *Filter* > *Custom* option
    -   Under "Data Format" define correct separator (at Sprint, a space); check output is correct under "Preview"
    -   Right click on window with live data frame and select *Plot Data* > *xy-Curve*

-   Other
    -   Firefox 104 power meter readouts
    -   Android provides information about which apps are draining the battery
    -   See Brendan Gregg's Blog "CPU Utilization is wrong": <www.brendangregg.com/blog/2017-05-09/cpu-utilization-is-wrong.html>
        -   AVX-512 instructions are more intensive, not a 1:1 correlation of CPU and energy consumption
    -   Using RAPL readout for Plasma desktop widget

## 15:00-15:30

-   Video call with virtual participant, discussion of the above

## 15:30-17:30

-   Testing the GUDE Python script
    -   Running update on System Under Test and recording energy and performance data
    -   Running Kate script for 3 iterations

-   Finalizing GUDE script, merging at FEEP repository: <https://invent.kde.org/teams/eco/feep/-/tree/master/tools/GUDEPowerMeter>

## 17:30-18:30

-   Gosund SP111 guide: see <https://invent.kde.org/teams/eco/feep/-/blob/master/tools/gosund-sp111-notes.md>
    -   Added section for MQTT setup
    -   Added section for switching WiFi networks
    -   Added section for recovering non-booting devices

-   Tested Gosund guide

<a id="org9f244ec"></a>

# WRAP UP

## 18:00-18:45

-   Wrap up, clean up, lock up at KDAB Berlin

Minutes for the call with the community on 2022-01-12.

Start: 19:00
End: 20:00

11 Participants + 1 BE4FOSS organizer [names removed to protect participant privacy]

Notes:

* Intro to the meeting, agenda, etc.
    * Topic: SoftAWERE project from Sustainable Digital Infrastructure Alliance
* Presentation
    * Joint project between SDIA and Öko-Institut
    * Focus on developer tools that are convenient and enjoyable to use (cf. inconvenience of measuring in lab)
    * Initial idea: integration into linting frameworks; later idea: do not just focus on the software itself, but on the libraries
    * Every lbirary could have indication of whether it is energy efficient or not (e.g., GitHub icons)
    * Goal: integrating energy consumption into decisions / choice hierarchies
    * Not interested in comparability: it is not a competition between libraries or a certification for libraries
    * Repeatability of the measurements is important so you can regularly check energy consumption
    * Idea of CPU benchmarking tools as indicator of energy consumption (measuring energy without measuring energy)
* Q&A/Comments (not exhaustive, intended as overview)
    * Q: How to use the tools from engineering perspective?
    * A: Execution environment locally (unit tests) vs. server environment (CI/CD runner), integrating API to expose energy consumption; indicator not precise; Laptop/desktop has dynamic power curve, idle CPU throlles down, but for SERVERS energy saving mode is shut off (running 100% power) = constant power consumption; a focus on server-side applications
    * Q: How is the project related to BE, also historically? Is the project involved in extending the BE crtieria to server-side applications? Is user autonomy criteria also of interest to the project?
    * A: Doesn't think so, but there will be policy recommendations. Focus is on  building tools for energy efficiency. Re user autonomy, only Open Source, with a focus on libraries used by everyone.
    * Participant Comment: Working on another project also looking into measuring energy consumption of server applications, specifically search requests. Interest in ARM servers, but limits in comparability to AMD64 architectures
    * Response: Welcome to join SDIA monthly meetup. Virtualization layer has to expose energy consumption, OpenStack/VMware working on it.
    * Participant Comment: Goal of total transparency: what data, what power!
    * Response: You will get pushback, since this will expose software to cost-benefit analysis. If can see actual carbon emissions, than it will not be zero when not including carbon certificates. And you rent a rack, pay for 5 kW even if you don't use it. Why pay for 5 kW if you only use 1 kW. Datacenters are run by real estate people, not IT people.
    * Need to convince data center managers.
    * Often, digitization is discussed as being able to solve all our problems, without discussing downsides. Example of 1.8TB of data on opening and closing trashcans. Question to ask: Does this make sense?
    * Q: When pricing resource usage precisely, how does it correlate to energy consumptuon?
    * A: No evidence for this. AWS just prices memory, the most expensive component of their service. Base price for CPU, with memory priced on top. No correlation with physical resource consumption.
    * Q: Integrating into CI pipeline, an API would be fantastic for this. Could be unique selling point for people like us!
    * A: I want it to be simple. Developers often get paid for building something, not for energy efficiency. They will say: Not my job.
    * Q: How far away are we from something like that?
    * A: Not so far. Currently building a rack to make CI/CD environments accessible (with all heat recovered into lived-in houses).
    * Q: Deriving energy consumption from a code linter. Any experience with that? Asking because in own research found no correlation between linter warnings and energy consumption.
    * A: We also had the hypothesis of code complexity being correlated with energy consumption, but it was an insignificant effect, we did not find this. Run unit tests or test suite to measure power consumption while the test is running. More accuracy there than linter tests.
    * In university environment, challenge from IT professors. JVM/compiler can make bad code go away, and that's the challenge. Also inverse is true: inefficient code but compiler can do nothing about it, not something a compiler or linter could have found.
    * Also need to discuss application modernization: legacy systems, don't know which servers still have data, so keep them around.
    * Q: How did you get such a wide range of sponsers?
    * A: Economic case for sustainability, and this is more desireable for companies. But sponsors have no influence on decisions, and everything we do is open source.
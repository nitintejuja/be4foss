# Minutes for the community meetup on 2022-11-09.

## Details

* Start: 19:00 CET; End: 20:00 CET

* 6 online participants + 2 BE4FOSS organizers [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Arne from Green Coding Berlin will present tooling to find and evaluate the current configuration state of a machine regarding performance-relevant settings. 

* *Details*: For the [Green Metrics Tool](https://github.com/green-coding-berlin/green-metrics-tool) the Green Coding Berlin team have developed some tooling to find and judge the current configuration state of a machine regarding performance-relevant settings, namely stuff like TurboBoost, Hyper-Threading etc. For work with the open-source group 'boavizta' the team are currently trying to glue this into the [Energizta project](https://github.com/Boavizta/Energizta/), which aims to be an open alternative to proprietary databases like SPECPower. Arne will present the tool for evaluating certain machine configurations and how to create a benchmarking tool that puts energy as a metric first.

# Notes

* Introductions in the group
* Intro to presenter
    * Presentation: LINK
    * Roadmap (see slides): (I) Intro, (II) Getting to know energy as a metric, (III) Identifying further metrics, (IV) Some info on RAPL and other tools

## (I) Intro to what Green Coding Berlin does

* Measures software in different architecture & hardware configurations, understand the impact on performance & energy consumption, open source data, build open source tooling
* What we need is an open database with energy profiles (currently SPECPower proprietary benchmark, no verification of data; interesting projects Boavizta Energizta, KDE's FEEP)
* Entry level talk how settings effect power of a benchmarking run

## (II) Getting to know energy as a metric

* OOB system, how reliable is anyway a metric?
* Using Ubuntu 22.04, using `sysbench` (10,000 fixed events, 4 cores ...), `perf`
* Usually a process runs in the same time, compare `sysbench` with fixed operations
* Using `RAPL` to get energy measurement of CPU. No graphic card involved just looking at the CPU, 10 runs need same time but energy increases over time and drops in between.
    * Why is energy increasing but time is not? Why does it drop at certain intervals?
* The voltage needed to operate a transistor increases quadratically with the temperature -- it needs more energy to do the same work
    * OS can put a process on a different core with a colder temperature -- why values go down
    * If you wait between each measurement, variation goes down
    * Logging temp of system can account for this effect
* Utilization anomalies in CPU metrics, processor and operating system can both set the frequency higher or lower, so it is very hard to set up a reliable system, you have to know what is going on at all times
* Can energy splitting save us? Use time as a splitting mechanism and instructions to split the energy signal
* Inspecting code on an instructional level, no `perf` counter multiplexing, instruction share and jiffy share can deviate; What way of splitting is better? Time or instruction splitting? Hypothesis (see slides) 
    * Can see how many lines have been written (7500 lines)
    * Running a separate process additionally using `stress-ng`
    * What you see is that `top` reports a lower CPU utilization although it has written more lines
    * Which hidden variable was not measured? DVFS
    * Time is a problem
* Or maybe CPU cycles? Does the amount of jiffies per wall-time change when the CPU frequency changes?
    * Same active and total time but different amount of events.
    * Amount does not change
* Inspecting code on an instruction level
    * E.g., calculate prime numbers for 10 seconds, and log amount of instructions in that time
    * Log instructions for process and for whole OS
    * If whole system has used 190 joules, can we split down to time (system was active for X amount of time) or should we take the ratio for the instructions (74 billion to 15 billion)?
    * Difference of 30%
    * Time is an arbitrary thing to use: perhaps better to look at number of instructions
* Inspecting code on an instruction level to see if time and instruction count differ. First insight (see slides):
    * Timeout of 3 seconds
    * Energy is very similar, but the instructions are very different
    * Maybe instructions are not the relevant metric
    * Assumption: time and energy are linear, but maybe due to being in idle state 

## III Identifying further metrics
* SPECPower databases where vendors publicize results and know which metrics to tune, how do you find out which variables are at play
* TurboBoost on/off changes performance to power ratio
* Settings: Hyperthreading, SVM/VT-X/VT-D/AMD-V, hardware prefetchers, P-states, C-states, memory speed and timings, turbo boost (more on the slides)
* Inspecting hyperthreading: Does on/off change energy consumption?
    * Fixed time run, not more calculations can be done, when activated additional virtual cores can be added, more calculations are possible, energy usage rises
    * Logical limit at 4 cores
    * Should we turn it on or off? Joules needed per operation is lower when it is on and more threads are running
* Inspecting Turbo Boost:
    * Turning turboboost on means the performance-to-power ratio increases
    * Fixed amount of calculation and fixed time run, enabled more energy is used but time used is less; disabled uses less energy although it took longer to make the calculation (9.5% longer)
    * So on 1 core it makes sense to turn it on; ith 4 cores disabling it for 1 core makes sense
    * Can you turn the system off due to being faster to save energy? It depends on how much is saved (see slides)

## (IV) Some infos on RAPL and other tools
* Kepler in distributed systems: www.sustainable-computing.io
* RAPL studies (see slides) it is very accurate in measuring but timing inaccuracies (not always in fixed intervals of time)
* Questions to community: What other settings do you know? Do you know what cloud vendors/hosters typically tune/configure? Do you know of any systems that are particularly good for benchmarking?

## Q(uestions)/A(nswers)/C(omments):

* Q: Insights on turning Turbo Boost and hyper threading on and off and impact on variation in measurements?
    * A: Hyperthreading is typically turned on because it gives you more threads, but in system that needs low latency hyperthreading is off. But no data on SD for measurements.
    * C: Turboboost apparently has an impact on variance.
    * A: Yes, there is information about that. No deterministic point when turboboost kicks in. If CPU is always running at max, it increases linear in the amount of work it can do. If schedule tool is on, it creeps down until 70% of load, and then it increases all of a sudden after that.
* C: Amount of time vs. amount of workload, similar to in master thesis: measuring the energy has lower variation than measuring CPU usage!
    * A: Depends on workload you have. Hidden variables like sleep. Interesting how energy stays put, although you do not know what exactly is going on. Have you tried to use a locked down system?
    * C: I used a bare minimum setup. CPU usage is relatively low for apps measured (i.e., Kate, Okular). Quite a bit of idle mode in there and so CPU is not fully utilized.
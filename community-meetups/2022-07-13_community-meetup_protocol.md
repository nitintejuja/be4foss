# Minutes for the call with the community on 2022-07-13.

## Details

* Start: 19:00 CEST; End: 20:00 CEST

* 8 Participants + 2 BE4FOSS organizers [names removed to protect participant privacy]

* *Topic*: Arne Tarara from Green Coding Berlin presented his team's green metrics tool (AGPL v.3)

* *Details*: The project Green Coding Berlin [1] will present their green metrics tool (AGPL v.3) [2] to our community. Using a container-based approach, their tool differs from the lab setup the KDE Eco community is focused on currently. You can find a short example of what it does at the FEEP repository. [3] 
    + [1] https://www.green-coding.org/ 
    + [2] https://github.com/green-coding-berlin/green-metrics-tool 
    + [3] https://invent.kde.org/teams/eco/feep/-/blob/master/tools/green-metrics-tool_green-coding-berlin.md

* From the GitHub repository:
> The software can orchestrate Docker containers according to a given specification in a usage-flow.json file.
> These containers will be setup on the host system and the testing specification in the usage-flow will be run by sending the commands to the containers accordingly.
> During this process the performance metrics of the containers are read through the stream of docker stats. 

## Notes

* Round of introductions from participants
* Intro to the presenter and topic.
    * Presentation: 2022-07-13_community-meetup_presentation_Tarara_Green-Metrics-Tool.pdf
* Open source toolchain: APGLv3 licensed
* Reusing existing infrastructure as code, modularize it as containers, tool needs Docker images to run, multiple docker files for multiple applications
* Focusing on small modular elements that can be used again
* Tool allows parallel measurements of internal and external measurements: Measure energy through integrated parts (internal voltage regulator on CPU, RAPL?); also DC measurements by cutting into power lines (to falsify measurements)
* Designed to work with different kind of applications, but main focus is on web applications
* Tool applies best practices (e.g., burn-in phase)
* Toolchain (open api), database available via web interface, also include Jupyter notebooks; for illustration of how to interact with API, see slides
* Metrics reported: averages of cpu, etc; including charts
* For an image of architecture, see slides:
    * At the moment only Docker, but working on integrating others (Kubernetes)
    * Flow: already have E2E tests
    * `xdotool` can be run in shell scripts
* Standard-Usage-Scenarios are pinned through Github repositories to allow reproducibility
* Presenter demonstrates the tool (see slides): place yml file in a folder on Github, set the parameters
    * For example yml file, see slides
* For debug purposes can use `curl`, puppeteer starts, see what browser has done (example was minimal), can run browser headless or headfull
* Short term: X11 applications, Linux working, new DC measurement; Long term: Windows and Android
* Team wants to re-measure Okular using the SUS to compare results
* Examples on the website:
    * Metrics: https://metrics.green-coding.org
    * Example applications: https://github.com/green-coding-berlin/simple-example-application
    * Website and blog and newsletter https://www.green-coding.org
    * Meetup group: https://www.meetup.com/green-coding
    * Demo Open Data Repository: https://github.com/green-coding-berlin/simple-example-application
    * The tool: https://github.com/green-coding-berlin/green-metrics-tool
    * Arne Tarara: https://www.linkedin.com/in/arne-tara, arne@green-coding.org
* Q&A/Comments
    * C: Community member's work on hardware sensors: https://invent.kde.org/davidhurka/usb-spi-power-sensors
    * Q: What are the main benefits or drawbacks in comparison to external power meter measurements? A: It scales better as a software solution; external power meter measurements use AC measurement device as single source of truth; top-down approach, people want to know their carbon footprint, for example how much do they consume in the cloud; it has to be seen long term what works better or is more accurate
    * Q: Can you say more about best practices? A: Burn in time; low variance; emulating network request with a proxy (network energy estimated after the fact); idle time before and after
    * C: Idea of starting a joined wiki about the landscape of software consumption measuurements (what works, what not, etc.)
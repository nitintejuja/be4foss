# Usage Scenario Implementation

## xdotool

xdotool provides mouse and keyboard input emulation

### Mouse click:
```
xdotool mousemove 100 200 # Move mouse 100px left and 200px down from top-left screen corner
xdotool click 1 # left click (2 for middle, 3 for right mouse button)
```

### Mouse drag:
```
xdotool mousemove 200 300 # Move
xdotool mousedown 1 # Press
xdotool mousemove 400 400 # Move
xdotool mouseup 1 # Release
```

### Keyboard Shortcut
```
xdotool key Ctrl+g
```

### Typing text
```
xdotool type "KDE is awesome"
```

## Tips for implementation

### bash scripts

A bash script is a list of shell commands. Can also include conditions, loops, output etc.

```
#!/usr/bin/env bash

echo "Starting"

xdotool mousemove 100 200
xdotool click 1

sleep 5

xdotool key Ctrl+g

echo "Done"

```

### Live mouse positions

Figuring out the correct coordinates for a specific control can be cumbersome. Something that can help is printing out the current mouse position in real time
```
while true; do; xdotool getmouselocation; done
```

### Configurable shortcuts

Most KDE apps allow to configure shortcuts, including assigning shortcuts to menu items that don't have one by default

Settings > Configure Keyboard Shortcuts

## KXmlGui

KXmlGui maintains an internal registry of 'Actions'. Most actions correspond to a menu item. It powers features like configurable shortcuts, editable toolbars, HUD-style KCommandBar.

Internally saved as an XML file, hence the name.

Actions have an id, e.g. "File > New" is "file_new"

Actions can be triggered from the outside using D-BUS. qdbus is a commandline tool for using D-Bus.

List actions:
```
qdbus org.kde.kate-$(pidof kate) /kate/MainWindow_1 org.kde.KMainWindow.actions
```

Activate 'file_new'
```
qdbus org.kde.kate-$(pidof kate) /kate/MainWindow_1 org.kde.KMainWindow.activateAction file_new
```

